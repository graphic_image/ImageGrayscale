﻿namespace _2图像灰度化
{
    partial class Form1
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.open = new System.Windows.Forms.Button();
            this.save = new System.Windows.Forms.Button();
            this.close = new System.Windows.Forms.Button();
            this.pixel = new System.Windows.Forms.Button();
            this.memory = new System.Windows.Forms.Button();
            this.pointer = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.timeBox = new System.Windows.Forms.TextBox();
            this.min = new System.Windows.Forms.Button();
            this.max = new System.Windows.Forms.Button();
            this.er值化127 = new System.Windows.Forms.Button();
            this.avgErzhihua = new System.Windows.Forms.Button();
            this.Bernsen = new System.Windows.Forms.Button();
            this.oust二值化 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // open
            // 
            this.open.Location = new System.Drawing.Point(37, 12);
            this.open.Name = "open";
            this.open.Size = new System.Drawing.Size(75, 23);
            this.open.TabIndex = 0;
            this.open.Text = "打开图像";
            this.open.UseVisualStyleBackColor = true;
            this.open.Click += new System.EventHandler(this.open_Click);
            // 
            // save
            // 
            this.save.Location = new System.Drawing.Point(37, 41);
            this.save.Name = "save";
            this.save.Size = new System.Drawing.Size(75, 23);
            this.save.TabIndex = 1;
            this.save.Text = "保存图像";
            this.save.UseVisualStyleBackColor = true;
            this.save.Click += new System.EventHandler(this.save_Click);
            // 
            // close
            // 
            this.close.Location = new System.Drawing.Point(37, 70);
            this.close.Name = "close";
            this.close.Size = new System.Drawing.Size(75, 23);
            this.close.TabIndex = 2;
            this.close.Text = "关闭";
            this.close.UseVisualStyleBackColor = true;
            this.close.Click += new System.EventHandler(this.close_Click);
            // 
            // pixel
            // 
            this.pixel.Location = new System.Drawing.Point(37, 117);
            this.pixel.Name = "pixel";
            this.pixel.Size = new System.Drawing.Size(75, 23);
            this.pixel.TabIndex = 3;
            this.pixel.Text = "提取像素法";
            this.pixel.UseVisualStyleBackColor = true;
            this.pixel.Click += new System.EventHandler(this.pixel_Click);
            // 
            // memory
            // 
            this.memory.Location = new System.Drawing.Point(37, 146);
            this.memory.Name = "memory";
            this.memory.Size = new System.Drawing.Size(75, 23);
            this.memory.TabIndex = 4;
            this.memory.Text = "内存法";
            this.memory.UseVisualStyleBackColor = true;
            this.memory.Click += new System.EventHandler(this.memory_Click);
            // 
            // pointer
            // 
            this.pointer.Location = new System.Drawing.Point(37, 175);
            this.pointer.Name = "pointer";
            this.pointer.Size = new System.Drawing.Size(75, 23);
            this.pointer.TabIndex = 5;
            this.pointer.Text = "指针法";
            this.pointer.UseVisualStyleBackColor = true;
            this.pointer.Click += new System.EventHandler(this.pointer_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(35, 201);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(65, 12);
            this.label1.TabIndex = 6;
            this.label1.Text = "运行时间：";
            // 
            // timeBox
            // 
            this.timeBox.Location = new System.Drawing.Point(37, 216);
            this.timeBox.Name = "timeBox";
            this.timeBox.Size = new System.Drawing.Size(75, 21);
            this.timeBox.TabIndex = 7;
            this.timeBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // min
            // 
            this.min.Location = new System.Drawing.Point(37, 243);
            this.min.Name = "min";
            this.min.Size = new System.Drawing.Size(75, 23);
            this.min.TabIndex = 8;
            this.min.Text = "最小值法";
            this.min.UseVisualStyleBackColor = true;
            this.min.Click += new System.EventHandler(this.min_Click);
            // 
            // max
            // 
            this.max.Location = new System.Drawing.Point(37, 272);
            this.max.Name = "max";
            this.max.Size = new System.Drawing.Size(75, 23);
            this.max.TabIndex = 9;
            this.max.Text = "最大值法";
            this.max.UseVisualStyleBackColor = true;
            this.max.Click += new System.EventHandler(this.max_Click);
            // 
            // er值化127
            // 
            this.er值化127.Location = new System.Drawing.Point(37, 310);
            this.er值化127.Name = "er值化127";
            this.er值化127.Size = new System.Drawing.Size(75, 23);
            this.er值化127.TabIndex = 10;
            this.er值化127.Text = "127二值化";
            this.er值化127.UseVisualStyleBackColor = true;
            this.er值化127.Click += new System.EventHandler(this.er值化127_Click);
            // 
            // avgErzhihua
            // 
            this.avgErzhihua.Location = new System.Drawing.Point(37, 339);
            this.avgErzhihua.Name = "avgErzhihua";
            this.avgErzhihua.Size = new System.Drawing.Size(75, 23);
            this.avgErzhihua.TabIndex = 11;
            this.avgErzhihua.Text = "平均值二值化";
            this.avgErzhihua.UseVisualStyleBackColor = true;
            this.avgErzhihua.Click += new System.EventHandler(this.avgErzhihua_Click);
            // 
            // Bernsen
            // 
            this.Bernsen.Location = new System.Drawing.Point(37, 368);
            this.Bernsen.Name = "Bernsen";
            this.Bernsen.Size = new System.Drawing.Size(88, 23);
            this.Bernsen.TabIndex = 12;
            this.Bernsen.Text = "Bernsen算法";
            this.Bernsen.UseVisualStyleBackColor = true;
            this.Bernsen.Click += new System.EventHandler(this.Bernsen_Click);
            // 
            // oust二值化
            // 
            this.oust二值化.Location = new System.Drawing.Point(37, 397);
            this.oust二值化.Name = "oust二值化";
            this.oust二值化.Size = new System.Drawing.Size(75, 23);
            this.oust二值化.TabIndex = 13;
            this.oust二值化.Text = "ostu二值化";
            this.oust二值化.UseVisualStyleBackColor = true;
            this.oust二值化.Click += new System.EventHandler(this.oust二值化_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(792, 566);
            this.Controls.Add(this.oust二值化);
            this.Controls.Add(this.Bernsen);
            this.Controls.Add(this.avgErzhihua);
            this.Controls.Add(this.er值化127);
            this.Controls.Add(this.max);
            this.Controls.Add(this.min);
            this.Controls.Add(this.timeBox);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.pointer);
            this.Controls.Add(this.memory);
            this.Controls.Add(this.pixel);
            this.Controls.Add(this.close);
            this.Controls.Add(this.save);
            this.Controls.Add(this.open);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.Form1_Paint);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button open;
        private System.Windows.Forms.Button save;
        private System.Windows.Forms.Button close;
        private System.Windows.Forms.Button pixel;
        private System.Windows.Forms.Button memory;
        private System.Windows.Forms.Button pointer;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox timeBox;
        private System.Windows.Forms.Button min;
        private System.Windows.Forms.Button max;
        private System.Windows.Forms.Button er值化127;
        private System.Windows.Forms.Button avgErzhihua;
        private System.Windows.Forms.Button Bernsen;
        private System.Windows.Forms.Button oust二值化;
    }
}

