﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Runtime.InteropServices;
using System.ComponentModel;
using System.Threading;

namespace _2图像灰度化
{
    class HiPerfTimer//时间精确到1ms
    {
        [DllImport("Kernel32.dll")]
        private static extern bool QueryPerformanceCounter(
            out long lpPerformanceCount);

        [DllImport("Kernel32.dll")]
        private static extern bool QueryPerformanceFrequency(
            out long lpFrequency);
        private long startTime, stopTime;
        private long freq;
        public HiPerfTimer()
        {
            startTime = stopTime = 0;
            freq = 0;
          if(QueryPerformanceFrequency(
            out freq)==false)//获取cpu频率
            {
                throw new Win32Exception();//不支持高性能计时器
            }
        }

        public void Start()
        {
            Thread.Sleep(0);
            QueryPerformanceCounter(out startTime);
        }
        public void Stop()
        {
            QueryPerformanceCounter(out stopTime);
        }
        public double Duration
        {
            get
            {
                return (double)(stopTime - startTime) * 1000 / (double)freq;
            }
        }
    }
}
